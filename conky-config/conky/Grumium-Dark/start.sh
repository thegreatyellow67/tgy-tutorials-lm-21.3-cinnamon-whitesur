#!/bin/bash

killall conky
sleep 2s
		
conky -c $HOME/.config/conky/Grumium-Dark/Grumium-Dark.conf &> /dev/null &
