- Tema Whitesur per Firefox:
https://addons.mozilla.org/it/firefox/addon/whitesur-gtk-inspired-theme/?utm_source=addons.mozilla.org&utm_medium=referral&utm_content=search

- Tabs di Firefox più basse:
https://leochavez.org/index.php/2023/03/06/make-firefoxs-tabs-smaller/

- Estensione Tabliss:
https://addons.mozilla.org/en-US/firefox/addon/tabliss/

Impostazioni di sistema -> Schermata di accesso

NOTA: lo sfondo personalizzato per il login grafico è installato tramite install-res.sh

Sfondo: /usr/share/backgrounds/login-window/background_02_it.png
Tema GTK:				Whitesur-Dark
Tema delle icone:		Whitesur-dark
Puntatore del mouse:	Whitesur Cursors

La modifica del file slick-greeter.conf per aggiungere il tipo di carattere va fatta SOLO DOPO aver modificato le impostazioni della schermata di accesso (il file viene creato dopo aver modificato le impostazioni della stessa schermata di accesso):

$ sudo nano /etc/lightdm/slick-greeter.conf
font-name=SF Pro 9

- Installare il tema icone Breeze per LibreOffice:
sudo apt install libreoffice-style-sifr

