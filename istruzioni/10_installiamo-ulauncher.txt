- Installazione di Ulauncher:
$ cd ~/Scaricati/lm-21.3-cinnamon-whitesur
$ cd ulauncher
$ sudo dpkg -i ulauncher_5.15.6_all.deb; sudo apt install -fy

- Tema WhiteSur per Ulauncher:
Fonte: https://github.com/Raayib/WhiteSur-Dark-ulauncher

NOTA: il tema WhiteSur Dark è installato tramite install-res.sh

- Modifica impostazioni di Ulauncher:
Color theme: WhiteSur Dark
Launch at login [SPUNTATO]
Show all apps [SPUNTATO]

